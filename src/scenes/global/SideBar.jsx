import { useState } from "react";
import { ProSidebar, Menu, MenuItem } from "react-pro-sidebar";
import { Box, IconButton, Typography, useTheme } from "@mui/material";
import "react-pro-sidebar/dist/css/styles.css";
import Intro from '../../assets/intro.png'
import { Link } from "react-router-dom";
import { colorTokens } from "../../theme";
import { HomeOutlined, PeopleOutlined, ContactsOutlined, 
	ReceiptOutlined, PersonOutlined, CalendarTodayOutlined, MapOutlined,
	HelpOutlineOutlined, BarChartOutlined, PieChartOutlineOutlined, 
TimelineOutlined, MenuOutlined} from '@mui/icons-material';


const Item = ({ title, to, icon, selected, setSelected }) => {
  const theme = useTheme();
  const colors = colorTokens(theme.palette.mode);
  return (
    <MenuItem
      active={selected === title}
      style={{
        color: colors.grey[100],
      }}
      onClick={() => setSelected(title)}
      icon={icon}
    >
      <Typography>{title}</Typography>
      <Link to={to} />
    </MenuItem>
  );
};

const SideBar = () => {
	const theme = useTheme();
	const colors = colorTokens(theme.palette.mode);
	const [isCollapsed, setIsCollapsed] = useState(false);
	const [selected, setSelected] = useState("Dashboard");

	return (
		<Box sx={{
			"& .pro-sidebar-inner": {
			background: `${colors.primary[400]} !important`,
			},
			"& .pro-icon-wrapper": {
			backgroundColor: "transparent !important",
			},
			"& .pro-inner-item": {
			padding: "5px 35px 5px 20px !important",
			},
			"& .pro-inner-item:hover": {
			color: "#868dfb !important",
			},
			"& .pro-menu-item.active": {
			color: "#6870fa !important",
			},
		}}>
			<ProSidebar collapsed={isCollapsed}>
				<Menu iconShape="square">
					<MenuItem
					onClick={ () => setIsCollapsed(!isCollapsed)}
					icon={isCollapsed ? <MenuOutlined /> : undefined}>
						{!isCollapsed && (
						<Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                ml="15px">
                <Typography variant="h3" color={colors.grey[100]}>
					ADMINS
                </Typography>
                <IconButton onClick={() => setIsCollapsed(!isCollapsed)}>
                <MenuOutlined />
                </IconButton>
            </Box>
            )}
					</MenuItem>
				
	
		{!isCollapsed && (
		<Box mb="25px">
			<Box display="flex" justifyContent="center" alignItems="center">
				<img alt="profile-user" width="100px"
					height="100px"
					src={Intro}
					style={{ cursor: "pointer", borderRadius: "50%" }}
				/>
			</Box>
			<Box textAlign="center">
				<Typography
					variant="h2" color={colors.grey[100]} fontWeight="bold"
					sx={{ m: "10px 0 0 0" }}>
					Melly G.
				</Typography>
				<Typography
					variant="h5" color={colors.greenAccent[500]}>
						VIP Admin
				</Typography>
			</Box>
		</Box>
		)}
		


		<Box paddingLeft={isCollapsed ? "0%" : "10%"} 
		style={{listStyle:"none"}}>
			<Item
			title="Dashboard" to="/"
			icon={<HomeOutlined />} selected={selected} setSelected={setSelected}>
			</Item>
			<Typography variant="h6" color={colors.grey[300]} sx={{ m: "15px 0 5px 20px" }}>
				Data
			</Typography>
			<Item title="Team" to="/team" 
			icon={<PeopleOutlined />} selected={selected} setSelected={setSelected}/>
			<Item title="Contacts" to="/contacts" 
			icon={<ContactsOutlined />} selected={selected} setSelected={setSelected}/>
			<Item title="Invoices" to="/invoices" 
			icon={<ReceiptOutlined />} selected={selected} setSelected={setSelected} />
			<Typography variant="h6" color={colors.grey[300]} sx={{ m: "15px 0 5px 20px" }}>
				Pages
			</Typography>
			<Item title="Profile Form" to="/form" 
			icon={<PersonOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="Calendar" to="/calendar" 
			icon={<CalendarTodayOutlined/>} selected={selected} setSelected={setSelected} />
			<Item title="FAQ" to="/faq" 
			icon={<HelpOutlineOutlined />} selected={selected} setSelected={setSelected} />
			<Typography variant="h6" color={colors.grey[300]} sx={{ m: "15px 0 5px 20px" }}>
				Charts
			</Typography>
			<Item title="BarChart" to="/bar" 
			icon={<BarChartOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="LineChart" to="/line" 
			icon={<TimelineOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="Piechart" to="/pie" 
			icon={<PieChartOutlineOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="GeograpyChart" to="/geography" 
			icon={<MapOutlined />} selected={selected} setSelected={setSelected} />
		</Box>
		</Menu>

		</ProSidebar>
		</Box>
	)
}

export default SideBar




/*


const Item = ({ title, to, icon, selected, setSelected }) => {
	const theme = useTheme();
	const colors = colorTokens(theme.palette.mode);
	return(
		<MenuItem active={selected === title} 
			style={{color: colors.grey[100], cursor:"pointer"}}
			onClick={ () => setSelected(title)}
			icon={icon} >
			<Typography>{title}</Typography>
			<Link to={to} />
		</MenuItem>
	)
}


const SideBar = () => {
	const theme = useTheme();
	const colors = colorTokens(theme.palette.mode);
	const [isCollapsed, setIsCollapsed] = useState(false);
	const [selected, setSelected] = useState("Dashboard");

	return (
		<Box sx={{
			"& .pro-sidebar-inner": {
			background: `${colors.primary[600]} !important`,
			},
			"& .pro-icon-wrapper": {
			backgroundColor: "transparent !important",
			},
			"& .pro-inner-item": {
			padding: "5px 35px 5px 20px !important",
			},
			"& .pro-inner-item:hover": {
			color: "#868dfb !important",
			},
			"& .pro-menu-item.active": {
			color: "#6870fa !important",
			},
		}}>
			<ProSidebar collapsed={isCollapsed}>
				<Menu iconShape="square">
					<MenuItem
					onClick={ () => setIsCollapsed(!isCollapsed)}
					icon={isCollapsed ? <MenuOutlined /> : undefined}>
						{!isCollapsed && (
						<Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                ml="15px">
                <Typography variant="h3" color={colors.grey[100]}>
                ADMINIS
                </Typography>
                <IconButton onClick={() => setIsCollapsed(!isCollapsed)}>
                <MenuOutlined />
                </IconButton>
            </Box>
            )}
					</MenuItem>
				</Menu>
	
		{!isCollapsed && (
		<Box mb="25px">
			<Box display="flex" justifyContent="center" alignItems="center">
				<img alt="profile-user" width="100px"
					height="100px"
					src={Intro}
					style={{ cursor: "pointer", borderRadius: "50%" }}
				/>
			</Box>
			<Box textAlign="center">
				<Typography
					variant="h2" color={colors.grey[100]} fontWeight="bold"
					sx={{ m: "10px 0 0 0" }}>
					Melly G.
				</Typography>
				<Typography
					variant="h5" color={colors.greenAccent[500]}>
						VIP Admin
				</Typography>
			</Box>
		</Box>
		)}
		


		<Box paddingLeft={isCollapsed ? "0%" : "10%"} 
		style={{listStyle:"none"}}>
			<Item
			title="Dashboard" to="/"
			icon={<HomeOutlined />} selected={selected} setSelected={setSelected}>
			</Item>
			<Typography variant="h6" color={colors.grey[300]} sx={{ m: "15px 0 5px 20px" }}>
				Data
			</Typography>
			<Item title="Team" to="/team" 
			icon={<PeopleOutlined />} selected={selected} setSelected={setSelected}/>
			<Item title="Contacts" to="/contacts" 
			icon={<ContactsOutlined />} selected={selected} setSelected={setSelected}/>
			<Item title="Invoices" to="/invoices" 
			icon={<ReceiptOutlined />} selected={selected} setSelected={setSelected} />
			<Typography variant="h6" color={colors.grey[300]} sx={{ m: "15px 0 5px 20px" }}>
				Pages
			</Typography>
			<Item title="Profile Form" to="/form" 
			icon={<PersonOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="Calendar" to="/calendar" 
			icon={<PersonOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="FAQ" to="/faq" 
			icon={<HelpOutlineOutlined />} selected={selected} setSelected={setSelected} />
			<Typography variant="h6" color={colors.grey[300]} sx={{ m: "15px 0 5px 20px" }}>
				Charts
			</Typography>
			<Item title="BarChart" to="/bar" 
			icon={<BarChartOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="LineChart" to="/line" 
			icon={<TimelineOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="Piechart" to="/pie" 
			icon={<PieChartOutlineOutlined />} selected={selected} setSelected={setSelected} />
			<Item title="Calendar" to="/calendar" 
			icon={<CalendarTodayOutlined />} selected={selected} setSelected={setSelected} />
		</Box>


		</ProSidebar>
		</Box>
	)
}

export default SideBar */