import { Box, IconButton, useTheme, InputBase } from '@mui/material';
import { useContext } from 'react'
import { ColorModeContext, colorTokens } from '../../theme'
import { LightModeOutlined, DarkModeOutlined, 
	NotificationsOutlined, PersonOutlined, SettingsOutlined, Search} from '@mui/icons-material';

const TopBar = () => {
	const theme = useTheme();
	const colors = colorTokens(theme.palette.mode);
	const mode = useContext(ColorModeContext);

	return (
		<Box display="flex" justifyContent="space-between" p={2}>
			{/* SearchBox */}
			<Box display="flex" backgroundColor={colors.primary[400]} borderRadius="3px">
				<InputBase sx={{ ml: 2, flex: 1 }} />
				<IconButton>
					<Search sx={{}} />
				</IconButton>
			</Box>
			<Box display="flex" >
				<IconButton onClick={mode.toggleColorMode}>
					{theme.palette.mode === 'dark' ? (
						<DarkModeOutlined />
					) : (<LightModeOutlined />)}
				</IconButton>
				<IconButton>
					<NotificationsOutlined />
				</IconButton>
				<IconButton>
					<SettingsOutlined />
				</IconButton>
				<IconButton>
					<PersonOutlined />	
				</IconButton>
			</Box>
		</Box>
	)
}

export default TopBar