import { useState } from "react";
import FullCalendar from "@fullcalendar/react";
import { formatDate } from '@fullcalendar/core'
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import {Box,List,ListItem,ListItemText,Typography,useTheme} from "@mui/material";
import Header from "../../components/Header";
import { colorTokens } from "../../theme";

const Calendar = () => {
	const theme = useTheme();
	const colors = colorTokens(theme.palette.mode);
	const [currentEvents, setCurrentEvents] = useState([]);

	const handleDateClick = (selected) => {
		const title = prompt("enter a title for your event"); // TODO: change to the modal view!
		const calendarApi = selected.view.calendar;
		calendarApi.unselect();

		if(title){
			calendarApi.addEvent({
				id:`${selected.dateStr}-${title}`,
				title,
				start:selected.startStr,
				end:selected.endStr,
				allDay:selected.allDay,
			});
		}
	}

	const handleEventClick = (selected) => {
		if(window.confirm(`Are you sure to delete the event '${selected.event.title}`)){
			selected.event.remove();
		}
	}

	return (
	<Box m="20px">
		<Header title="Calendar" subtitle="Full Calendar" />
		<Box display="flex" justifyContent="space-between">
			{/* Calendar Events Side bar */}
			<Box flex="1 1 20%" backgroundColor={colors.primary[400]}
					p="15px" borderRadius="4px">
				<Typography variant="h5">
					Events
				</Typography>
				<List>
					{currentEvents.map( (e) => (
					<ListItem key={e.id}    sx={{backgroundColor: colors.greenAccent[500],margin: "10px 0",borderRadius: "2px",}}>
						<ListItemText  primary={e.title}
						secondary={
							<Typography>
							{formatDate(e.start, {
								year: "numeric",
								month: "short",
								day: "numeric",
							})}
							</Typography>
						}>
						</ListItemText>
					</ListItem>
					))}
				</List>
			</Box>
		{/* 	Actual Calendar */}
			<Box flex="1 1 100%" ml="15px">
			<FullCalendar
			height="75vh"
            plugins={[
            dayGridPlugin,
            timeGridPlugin,
            interactionPlugin,
            listPlugin,
            ]}
            headerToolbar={{
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
            }}
            initialView="dayGridMonth"
            editable={true}
            selectable={true}
            selectMirror={true}
            dayMaxEvents={true}
            select={handleDateClick}
            eventClick={handleEventClick}
            eventsSet={(events) => setCurrentEvents(events)}
            initialEvents={[
            {
                id: "1",
                title: "Dance",
                date: "2022-09-21",
            },
            {
                id: "2",
                title: "Dinner",
                date: "2022-09-28",
            },
            ]} />	
			</Box>			
		</Box>
	</Box>
)
}

export default Calendar